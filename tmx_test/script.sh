#!/bin/sh
tmux \
    new-session 'python3 file1.py' \; \
    rename-session ${PWD##*/} \; \
    split-window 'python3 file2.py' \; \
