import pandas as pd

url = 'https://coinmarketcap.com/currencies/dogecoin/historical-data/?start=20130428&end=20171123'

df = pd.read_html(url)

volume = df[0]['Volume'].replace('-', None).astype(int)

print(volume.mean())
print(volume.std())
print(volume.min())
print(volume.count())
print(volume.max())
