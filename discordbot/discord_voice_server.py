
import asyncio
import discord
import io
import logging
import os

logging.basicConfig(level=logging.INFO)

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
FILE_PATH = os.path.join(BASE_PATH, 'gtts.mp3')

if not os.path.exists(FILE_PATH):
    open(FILE_PATH, 'a').close()


if not discord.opus.is_loaded():
    # the 'opus' library here is opus.dll on windows
    # or libopus.so on linux in the current directory
    # you should replace this with the location the
    # opus library is located in and with the proper filename.
    # note that on windows this DLL is automatically provided for you
    discord.opus.load_opus('opus')


async def on_ready():
    await client.wait_until_ready()

    channel = discord.Object(id='414755754604757003')
    voice = await client.join_voice_channel(channel)

    start_time = os.path.getmtime(FILE_PATH)
    print('File Start Time:', start_time)

    while True:
        updated_time = os.path.getmtime(FILE_PATH)
        print('current time', updated_time)
        if updated_time > start_time:
            print('TTS File updated.')
            start_time = updated_time
            player = voice.create_ffmpeg_player(FILE_PATH)
            player.start()
            while not player.is_done():
                await asyncio.sleep(1)
        await asyncio.sleep(1)


client = discord.Client()
client.loop.create_task(on_ready())
client.run('NDE0NzU2MzA1MjQ5NjMyMjU2.DWsAQw.lOa3Q6KfSnVAfrDXteYv0o2wo7I')
