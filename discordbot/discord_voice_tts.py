import socket
import os
import random

from gtts import gTTS

from time import sleep

bind_ip = '0.0.0.0'
bind_port = 6666

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((bind_ip, bind_port))
server.listen(5)  # max backlog of connections

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
FILE_PATH = os.path.join(BASE_PATH, 'gtts.mp3')

print('Listening on {}:{}'.format(bind_ip, bind_port))


def create_tts_file(message):
    text = message
    print('Received', text)  # change this to logger
    language = random.choice(['en-uk', 'en-us', 'en-uk', 'en-au'])

    tts = gTTS(text=text, lang=language, slow=False)
    tts.save(FILE_PATH)


def handle_client_connection(client_socket):
    request = client_socket.recv(1024)
    create_tts_file('{}'.format(request.decode()))
    client_socket.send(b'ACK!')
    client_socket.close()
    sleep(2.5)

while True:
    client_sock, address = server.accept()
    print('Accepted connection from {}:{}'.format(address[0], address[1]))
    handle_client_connection(client_sock)
    sleep(1)
