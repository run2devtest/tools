import socket
import os

from google_speech import Speech
from time import sleep

bind_ip = '0.0.0.0'
bind_port = 6666

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((bind_ip, bind_port))
server.listen(5)  # max backlog of connections

print('Listening on {}:{}'.format(bind_ip, bind_port))


def echo(message):
    text = message
    print('Received', text)

    lang = "en-au"
    speech = Speech(text, lang)

    sox_effects = ("speed", ".92")
    speech.play(sox_effects)


def handle_client_connection(client_socket):
    request = client_socket.recv(1024)
    echo('{}'.format(request.decode()))
    client_socket.send(b'ACK!')
    client_socket.close()
    sleep(2.5)

while True:
    client_sock, address = server.accept()
    print('Accepted connection from {}:{}'.format(address[0], address[1]))
    handle_client_connection(client_sock)
    sleep(1)
