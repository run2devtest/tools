from telegram.ext import Updater, MessageHandler, Filters
from google_speech import Speech


def gettoken(name):
    with open(name, 'r') as TOKEN:
        return TOKEN.read().strip('\n').strip(' ')


def echo(bot, update):
    text = update.message.text
    print(text)

    lang = "en-au"
    speech = Speech(text, lang)

    sox_effects = ("speed", ".92")
    speech.play(sox_effects)
    print(bot)


def error(bot, update, error):
    """Log Errors caused by Updates."""
    print('Update "%s" caused error "%s"', update, error)


def main():
    updater = Updater(gettoken('CLIENT_TOKEN'))

    dp = updater.dispatcher

    dp.add_handler(MessageHandler(Filters.text, echo))

    dp.add_error_handler(error)

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
